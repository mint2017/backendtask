package com.lottostarlet.backendtest.app.persistence;

import com.lottostarlet.backendtest.app.model.GameResult;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GameResultRepository extends MongoRepository<GameResult, String> {

    GameResult findById(String id);
}
