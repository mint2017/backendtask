package com.lottostarlet.backendtest.app.domainlogic;

public interface TurnPlayer {

    Object getResultOfTurn();
}
